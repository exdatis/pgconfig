# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FrmConfig.ui'
#
# Created by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import configparser
import os

class Ui_FrmConfig(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self)
        self.setupUi(self)
        self.DEFAULT_CFG_FILE = 'pg.cfg'
        self.DEFAULT_SECTION = 'pg_config'
        self.SECTION_HOST = 'pg_host'
        self.SECTION_DB = 'pg_db'
        self.SECTION_USR = 'pg_usr'

    def setupUi(self, FrmConfig):
        FrmConfig.setObjectName("FrmConfig")
        FrmConfig.setWindowModality(QtCore.Qt.WindowModal)
        FrmConfig.resize(440, 177)
        self.gridLayout_2 = QtWidgets.QGridLayout(FrmConfig)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.pbCancel = QtWidgets.QPushButton(FrmConfig)
        self.pbCancel.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pbCancel.setObjectName("pbCancel")
        self.gridLayout_2.addWidget(self.pbCancel, 2, 0, 1, 1)
        self.pbSave = QtWidgets.QPushButton(FrmConfig)
        self.pbSave.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pbSave.setObjectName("pbSave")
        self.gridLayout_2.addWidget(self.pbSave, 2, 1, 1, 1)
        self.groupBox = QtWidgets.QGroupBox(FrmConfig)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.leHost = QtWidgets.QLineEdit(self.groupBox)
        self.leHost.setClearButtonEnabled(True)
        self.leHost.setObjectName("leHost")
        self.gridLayout.addWidget(self.leHost, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.leDb = QtWidgets.QLineEdit(self.groupBox)
        self.leDb.setClearButtonEnabled(True)
        self.leDb.setObjectName("leDb")
        self.gridLayout.addWidget(self.leDb, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.leUser = QtWidgets.QLineEdit(self.groupBox)
        self.leUser.setClearButtonEnabled(True)
        self.leUser.setObjectName("leUser")
        self.gridLayout.addWidget(self.leUser, 2, 1, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox, 1, 0, 1, 2)

        self.retranslateUi(FrmConfig)
        QtCore.QMetaObject.connectSlotsByName(FrmConfig)
        FrmConfig.setTabOrder(self.leHost, self.leDb)
        FrmConfig.setTabOrder(self.leDb, self.leUser)
        FrmConfig.setTabOrder(self.leUser, self.pbCancel)
        FrmConfig.setTabOrder(self.pbCancel, self.pbSave)

    def retranslateUi(self, FrmConfig):
        _translate = QtCore.QCoreApplication.translate
        FrmConfig.setWindowTitle(_translate("FrmConfig", "Pg konfiguracija"))
        self.pbCancel.setToolTip(_translate("FrmConfig", "Odbaci promene"))
        self.pbCancel.setText(_translate("FrmConfig", "&Odustani"))
        # pbCancel clicked
        self.pbCancel.clicked.connect(self.onCloseForm)

        self.pbSave.setToolTip(_translate("FrmConfig", "Kreiraj ini"))
        self.pbSave.setText(_translate("FrmConfig", "&Sačuvaj"))
        # pbSave clicked
        self.pbSave.clicked.connect(self.writeCnf)

        self.groupBox.setTitle(_translate("FrmConfig", " Podrazumevane vrednosti "))
        self.label.setText(_translate("FrmConfig", "Host(TCP/IP)"))
        self.label_2.setText(_translate("FrmConfig", "Default db"))
        self.leDb.setText(_translate("FrmConfig", "postgres"))
        self.label_3.setText(_translate("FrmConfig", "Default user"))

    def onCloseForm(self):
        """
        just close form
        :return:
        """
        quit()
    def prepareAll(self):
        """
        prepare data
        :return: None
        """
        self.readCnf()
    def readCnf(self):
        """
        Read configuration
        :param self:
        :return: None
        """
        # ini var

        def_host = 'PostgreSQL host'
        def_db = 'postgres'
        def_usr = 'postgres'
        # check if file exist
        if(os.path.isfile(self.DEFAULT_CFG_FILE)):
            # create parser
            my_cnf = configparser.ConfigParser()
            my_cnf.read(self.DEFAULT_CFG_FILE)
            def_host = my_cnf[self.DEFAULT_SECTION][self.SECTION_HOST]
            def_usr = my_cnf[self.DEFAULT_SECTION][self.SECTION_USR]
            def_db = my_cnf[self.DEFAULT_SECTION][self.SECTION_DB]
        # set values
        self.leHost.setText(def_host)
        self.leDb.setText(def_db)
        self.leUser.setText(def_usr)


    def writeCnf(self):
        """
        write configuration
        :param self:
        :return: None
        """
        # read var

        def_host = self.leHost.text()
        def_db = self.leDb.text()
        def_usr = self.leUser.text()

        # create parser
        my_cnf = configparser.ConfigParser()
        # set values
        my_cnf[self.DEFAULT_SECTION] = {}
        my_cnf[self.DEFAULT_SECTION][self.SECTION_HOST] = def_host
        my_cnf[self.DEFAULT_SECTION][self.SECTION_DB] = def_db
        my_cnf[self.DEFAULT_SECTION][self.SECTION_USR] = def_usr

        # Writing our configuration file to 'example.cfg'
        with open(self.DEFAULT_CFG_FILE, 'w') as config_file:
            my_cnf.write(config_file)
        # show message
        msg = "Podaci su uspešno sačuvani."
        caption = "Konfiguracioni fajl"
        # group-box as parent so position of dialogue will be the center of parent
        QtWidgets.QMessageBox.about(self.groupBox, caption, msg)
        self.onCloseForm()
