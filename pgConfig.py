#!/usr/bin/env python3
"""
    A simple configuration dialog to set default postgresql host,
    database an user.
    Useful to find available databases on the selected host
    Morar: feb 2015 <exdatis@yahoo.com>
"""
__author__ = 'morar'

from PyQt5.QtWidgets import QApplication, QWidget
from cnf import FrmConfig

if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    MainFrm= QWidget()
    MainFrmUi= FrmConfig.Ui_FrmConfig()
    MainFrmUi.setupUi(MainFrm)
    # read configuration
    MainFrmUi.prepareAll()
    # move to center of the screen
    MainFrm.move(app.desktop().screen().rect().center() - MainFrm.rect().center())

    MainFrm.show()
    sys.exit(app.exec_())
